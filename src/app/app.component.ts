import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {
  CurrencyPipe,
  DatePipe, JsonPipe,
  LowerCasePipe,
  NgClass,
  NgForOf,
  NgIf,
  NgStyle,
  NgSwitch,
  NgSwitchCase,
  NgSwitchDefault, PercentPipe, UpperCasePipe
} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {AppModule} from "./app.module";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NgIf, NgForOf, NgSwitchCase, NgSwitch, NgSwitchDefault, NgStyle, NgClass, FormsModule, LowerCasePipe, DatePipe, JsonPipe, CurrencyPipe, PercentPipe, UpperCasePipe, AppModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  providers: [DatePipe]
})


export class AppComponent implements OnInit{
  month: number=0;
  title = 'simpleCRM';
  success_msg = true;
  // success_flag = true;

  superPower = 'angular';
  tax = 90;

  styleProp = "purple";

  txtColor = 'red';

  styleClsProp = 'c3';

  conditionClsProp ='c6';

  getClsName(){
    return 'c3';
  }

  pageHeading = "Welcome to Santiago";

  pageCount = 10;

  userObject = { 'firstName': 'Chris', 'lastName': 'Ron'};

  isUserLoggedIn = false;

  colVal ="2"

  imgUrl = 'test.png';
  imgAlt = 'this is missing Image text';
  txtColorVal = 'red';

  username = "JoyBoy";

  // Built-In pipe examples

  lowerCaseExample = "NICE MAN";
  upperCaseExample = "learning phase";

  dateExample = Date.now();

  jsonExample = { username: "arc", major: "Angular", experience: 10};

  currencyExample = 125;

  percentExample = 0.6577;

  contacts = [
    {
      'firstName': 'john',
      'lastName': 'Doe',
      'contactId': 1234
    },

    {
      'firstName': 'dev',
      'lastName': 'joe',
      'contactId': 1212
    },

    {
      'firstName': 'don',
      'lastName': 'boe',
      'contactId': 1122
    },

    {
      'firstName': 'cr7',
      'lastName': 'best',
      'contactId': 7777
    },

    {
      'firstName': 'mbappe',
      'lastName': 'kil',
      'contactId': 9999
    },
  ];


  sayHello(){
    console.log("Hello from US");
  }

highlightBGcolor(){
    console.log("I am highlighted");
}

  constructor(){

  }

  ngOnInit(): void {

  }

}

