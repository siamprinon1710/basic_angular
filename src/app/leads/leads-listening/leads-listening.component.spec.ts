import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadsListeningComponent } from './leads-listening.component';

describe('LeadsListeningComponent', () => {
  let component: LeadsListeningComponent;
  let fixture: ComponentFixture<LeadsListeningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LeadsListeningComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LeadsListeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
