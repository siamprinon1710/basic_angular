// import {NgModule, provideZoneChangeDetection} from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterModule } from '@angular/router';
// import { routes } from './app.routes';
// import { FormsModule } from '@angular/forms';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import {provideServerRendering} from "@angular/platform-server";
// import {HashLocationStrategy, LocationStrategy} from '@angular/common';
// import {HTTP_INTERCEPTORS} from "@angular/common/http";
// import { AppRoutingModule } from './app-routing.module';
// import {ClientsComponent} from "./clients/clients.component";
//
// @NgModule({
//   declarations: [
//     ClientsComponent
//     // No need to declare AppComponent here as it's standalone
//     // Declare other components here if any
//   ],
//   imports: [
//     FormsModule,
//     BrowserModule,
//     BrowserAnimationsModule, // Add BrowserAnimationsModule for animations
//     RouterModule.forRoot(routes), AppRoutingModule, // Configure routes
//   ],
//
//   providers: [
//
//     {
//       provide: LocationStrategy, useClass: HashLocationStrategy
//     }
//
//     // // These should be provided as per their respective functionalities
//     // provideZoneChangeDetection({ eventCoalescing: true }), // Provide zone change detection with event coalescing
//     // provideServerRendering(), // Provide server-side rendering
//   ],
//   exports: [
//     ClientsComponent
//   ]
// })
// export class AppModule { }

// app.module.ts
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { ProductComponent } from './product/product.component';
import { ClientsComponent } from './clients/clients.component';
import {LoansComponent} from "./loans/loans.component";
import {LoanTypesComponent} from "./loan-types/loan-types.component";

// import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    ClientsComponent,


    // PageNotFoundComponent
  ],
  imports: [
    // BrowserModule,  // Ensure BrowserModule is only here
    AppRoutingModule,
    ProductComponent,
    LoansComponent,
    LoanTypesComponent
  ],
  providers: [],

  exports: [
    ClientsComponent
  ]
})
export class AppModule { }

