// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { Routes, RouterModule} from '@angular/router';
// import {ProductComponent} from "./product/product.component";
// import {ClientsComponent} from "./clients/clients.component";
//
//
//
// const routes: Routes = [
//   { path: 'product/:id', component: ProductComponent },
//   { path: 'product/:productId/photos/:photoId', component: ProductComponent },
//   {path: '/clients', component: ClientsComponent}
//
// ];
//
// @NgModule({
//   declarations: [],
//   imports: [
//     CommonModule,
//     RouterModule.forChild(routes)
//   ],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
//
//
//

// app-routing.module.ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { ClientsComponent } from './clients/clients.component';
// import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'product/:id', component: ProductComponent },
  { path: 'product/:productId/photos/:photoId', component: ProductComponent },
  { path: 'clients', component: ClientsComponent },
  { path: '', redirectTo: '/clients', pathMatch: 'full' },  // Default route
  // { path: '**', component: PageNotFoundComponent }  // Wildcard route for 404
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

