import {Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Router} from "express";

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [],
  templateUrl: './product.component.html',
  styleUrl: './product.component.scss'
})
export class ProductComponent implements OnInit{

  photoId =0;
  productId = 0;

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe((params) => {
      const interValue:any = params;

      this.photoId = interValue.photoId;
      this.productId = interValue.productId;

    });
  }


  ngOnInit(): void {

  }

}
